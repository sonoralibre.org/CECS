from django import http
from django.contrib import messages
from django.http import request
from .forms import GrupoForm, GrupoMateriaDocenteForm, MateriaDocenteForm, MateriaForm
from django.urls.base import reverse_lazy
from usuarios.models import Aspirante, Docente
from django.shortcuts import get_object_or_404, redirect, render
from .models import *
from django.views.generic import ListView, DetailView, TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import permission_required
from django.core.mail import EmailMessage
from django.template.loader import render_to_string

# Clase para visualizar la lista de aspirantes.
class lista_aspirantes(PermissionRequiredMixin,ListView):
    template_name='preinscripciones/list_aspirantes.html'
    model = Aspirante
    context_object_name="aspirantes"
    permission_required="auth.administrador_permiso"

# Funcion para visualizar la lista de aspirantes que han sido rechazados.
@permission_required('auth.administrador_permiso', raise_exception=True)
def lista_aspirantes_rechazados(request):
    lista_aspirantes=Aspirante.objects.filter(status=0)
    context={
        "aspirantes":lista_aspirantes
    }
    return render(request,"preinscripciones/list_aspirantes.html",context)

# Funcion para visualizar la lista de aspirantes que han sido aceptados.
@permission_required('auth.administrador_permiso', raise_exception=True)
def lista_aspirantes_aceptados(request):
    lista_aspirantes=Aspirante.objects.filter(status=1)
    context={
        "aspirantes":lista_aspirantes
    }
    return render(request,"preinscripciones/list_aspirantes.html",context)

# Funcion para visualizar a detalle los datos de un solo aspirante.
@permission_required('auth.administrador_permiso', raise_exception=True)
def ver_aspirante(request, pk):
    aspirante=get_object_or_404(Aspirante,id=pk)
    tipoDocumento=str(aspirante.comprobante_pago)[-3:]
    context={
        "aspirante":aspirante,
        "tipoDocumento":tipoDocumento
    }
    return render(request,"preinscripciones/detail_aspirante.html",context)

# Funcion que permite cambiar el estado de un aspirante a aceptado.
@permission_required('auth.administrador_permiso', raise_exception=True)
def aceptar_aspirante(request,pk):
    aspirante= get_object_or_404(Aspirante, id=pk)
    aspirante.status=1
    aspirante.is_active=True
    aspirante.save()
    enviar_correo_notificacion(pk)
    return redirect("preinscripciones:lista_aspirantes")

# Funcion para enviar un correo de notificación al aspirante cuando su cuenta para el sistema ha sido activada y ya puede iniciar sesión.
def enviar_correo_notificacion(pk):
    #Arreglar el envío de correo
    aspirante=Aspirante.objects.get(id=pk)
    mensaje =  render_to_string('aviso_aceptacion.html',
            {
                'aspirante' : aspirante
            }
        )
    asunto = 'Tu cuenta en CECS ha sido activada'
    correo = aspirante.email
    email = EmailMessage(
        asunto,
        mensaje,
        to=[correo]
    )
    email.content_subtype = 'html'
    email.send()

# Funcion que permite cambiar el estado de aceptado de un aspirante a no aceptado.
@permission_required('auth.administrador_permiso', raise_exception=True)
def rechazar_aspirante(request,pk):
    aspirante= get_object_or_404(Aspirante, id=pk)
    aspirante.status=0
    aspirante.is_active=False
    aspirante.save()
    return redirect("preinscripciones:lista_aspirantes")

# ---------------------- Grupos ----------------------------

# Clase para ver la lista de todos los grupos.
class ver_listas_grupos(PermissionRequiredMixin, ListView):
    permission_required="usuarios.aspirante_permiso"
    paginate_by = 5
    model = Grupo
    context_object_name="grupos"
    template_name='grupos/list_grupos.html'

# Clase para ver los datos a detalle de un grupo.
class ver_grupo(PermissionRequiredMixin, DetailView):
    permission_required="usuarios.aspirante_permiso"
    model = Grupo
    template_name="grupos/detalle.html"
    context_object_name='grupo'

# Clase que permite manejar el formulario de creación de un nuevo grupo.
class crearGrupo(PermissionRequiredMixin, CreateView):
    permission_required="auth.administrador_permiso"
    model=Grupo
    form_class=GrupoForm
    template_name="grupos/crear.html"
    success_url = reverse_lazy("preinscripciones:lista_grupos")

    def form_valid(self, form):
        form.instance.cupo=form.instance.cupo_limite
        return super(crearGrupo, self).form_valid(form)

# Clase que elimina un grupo en específico.
class eliminarGrupo(PermissionRequiredMixin, DeleteView):
    permission_required="auth.administrador_permiso"
    model = Grupo
    success_url = reverse_lazy('preinscripciones:lista_grupos')

# Clase que permite manejar los cambios de un formulario en un grupo existente.
class editarGrupo(PermissionRequiredMixin,UpdateView):
    permission_required="auth.administrador_permiso"
    model=Grupo
    form_class=GrupoForm
    template_name="grupos/editar.html"
    success_url=reverse_lazy("preinscripciones:lista_grupos")

# Funcion que permite ver la lista de alumnos en un grupo especifico.
@permission_required('usuarios.aspirante_permiso', raise_exception=True) #Nuevo permiso, para admin solamente
def ver_lista_alumnos_grupo(request,pk):
    grupoBusqueda=get_object_or_404(Grupo,id=pk)
    lista_aspirantes=Grupo_Aspirante.objects.filter(grupo=grupoBusqueda).values("aspirante","id")
    aspirantes=[]
    for aspirante in lista_aspirantes:
        aspirante_encontrado=get_object_or_404(Aspirante,id=aspirante['aspirante'])
        aspirante_encontrado.id_grupo_aspirante=aspirante['id']
        aspirantes.append(aspirante_encontrado)
    context={
        "aspirantes":aspirantes,
        "grupo":grupoBusqueda
    }
    return render(request,"grupos/list_aspirantes.html",context)

# Funcion que permite inscribir a un aspirante en un grupo.
@permission_required('usuarios.aspirante_permiso', raise_exception=True) #Cambiar el permiso, de momento está estático
#FALTA DE IMPLEMENTAR EN EL TEMPLATE Para que solamente los aspirantes se puedan inscribir
def inscribir_alumno_grupo(request, pk_aspirante, pk_grupo):
    aspirante=get_object_or_404(Aspirante, id=pk_aspirante)
    grupo=get_object_or_404(Grupo, id=pk_grupo)
    # Validacion para que se inscribia si solo tiene cupo.
    if(grupo.cupo<=0):
        messages.error(request,f'El grupo ya no tiene cupo')
        context={
            "messages":messages
        }
    elif(aspirante.grupo!=None): # Validacion para que si ya esta inscrito en algun grupo no se pueda inscribir en otro.
        messages.error(request,f'Ya estás inscrito en un grupo')
        context={
            "messages":messages
        }
    else:
        aspirante.grupo = grupo
        aspirante.save()
        Grupo_Aspirante.objects.create(grupo=grupo, aspirante=aspirante)
        cupos_usados=len(Grupo_Aspirante.objects.filter(grupo=grupo))
        grupo.set_cupos_restantes(cupos_usados=cupos_usados)
        messages.success(request,f'Inscrito correctamente en el grupo {grupo.letra}')
        context={
            "messages":messages
        }
    return redirect("preinscripciones:lista_grupos")

# ------------------------- Materias -----------------------------------------

# Clase que permite manejar el formulario para crear una nueva materia.
class NuevaMateria(PermissionRequiredMixin,CreateView):
    permission_required="auth.administrador_permiso"
    model=Materia
    form_class=MateriaForm
    template_name="materia/crear.html"
    success_url=reverse_lazy("preinscripciones:lista_materias")

# Clase que permite crear una relacion de una materia con su docente que la imparte.
class NuevaMateriaDocente(PermissionRequiredMixin,CreateView):
    permission_required="auth.administrador_permiso" #Nuevo permiso
    model=Materia_docente
    form_class=MateriaDocenteForm
    template_name="materia_docente/crear.html"
    success_url=reverse_lazy("preinscripciones:lista_materias")

# Metodo que permite ver la lista de materias con sus docentes que la imparten.
@permission_required("auth.administrador_permiso", raise_exception=True)
def lista_materias(request):
    materias=Materia.objects.all()
    for materia in materias:
        docentes_imparten=Materia_docente.objects.filter(materia=materia).values("docente")
        docentes=[]
        for docente in docentes_imparten:
            docenteEncontrado=Docente.objects.get(id=docente['docente'])
            docentes.append(docenteEncontrado)
        if len(docentes) != 0:
            materia.docentes=docentes
    context={
        "materias":materias
    }
    return render(request,"materia/lista.html",context)

# Metodo que permite ver los detalles de una materia en especifico.
@permission_required("usuarios.aspirante_permiso", raise_exception=True)
def ver_materia(request,id):
    materia=get_object_or_404(Materia,id=id)
    docentes_imparten=Materia_docente.objects.filter(materia=materia).values("docente")
    docentes=[]
    for docente in docentes_imparten:
        docenteEncontrado=Docente.objects.get(id=docente['docente'])
        docentes.append(docenteEncontrado)
    if len(docentes) != 0:
        materia.docentes=docentes
    context={
        "materia":materia
    }
    return render(request,"materia/detalle.html",context)

# Clase que permite crear una nueva relacion de un grupo con una materia impartida por un docente en especifico.
class AgregarGrupoMateria(PermissionRequiredMixin,CreateView):
    permission_required="auth.administrador_permiso"
    model=Grupo_materia_docente
    form_class=GrupoMateriaDocenteForm
    template_name="grupos/agregar_materia.html"
    success_url=reverse_lazy("preinscripciones:lista_grupos")

# Clase que permite manejar el formulario con los datos de una materia existente.
class EditarMateria(PermissionRequiredMixin,UpdateView):
    permission_required="auth.administrador_permiso"
    model=Materia
    form_class=MateriaForm
    template_name="materia/editar.html"
    success_url=reverse_lazy("preinscripciones:lista_materias")

# Funcion que permite ver las materias pertenecientes a un grupo en especifico.
@permission_required("usuarios.aspirante_permiso", raise_exception=True)
def verMateriasGrupo(request, id):
    #Valido si el grupo existe y lo recupero
    grupo=get_object_or_404(Grupo, id=id)
    #Recupero las materias que tiene el grupo
    materias=Grupo_materia_docente.objects.filter(grupo=grupo).values('materia_docente')
    materia_docente=[]
    #Recupero las materias con su docente
    for materia in materias:
        materia_docente_obtenido=Materia_docente.objects.get(id=materia['materia_docente'])
        materia_docente.append(materia_docente_obtenido)
    context={
        "materias":materia_docente,
        "grupo":grupo
    }
    return render(request, "grupos/lista_materias.html",context)

# Funcion que permite eliminar una relacion entre una materia impartida por un docente en especifico en un grupo en especifico.
@permission_required("auth.administrador_permiso", raise_exception=True)
def eliminarGrupoMateriaDocente(request, id_grupo, id_materia_docente):
    grupo=get_object_or_404(Grupo,id=id_grupo)
    materia_docente=get_object_or_404(Materia_docente,id=id_materia_docente)
    gmd=get_object_or_404(Grupo_materia_docente, grupo_id=id_grupo, materia_docente_id=id_materia_docente)
    gmd.delete()
    return verMateriasGrupo(request,id_grupo)

# Funcion que permite eliminar la realcion de que un profesor o docente imparta una materia en especifico.
@permission_required("auth.administrador_permiso", raise_exception=True)
def eliminar_materia_docente(request, id_materia, id_docente):
    materia=get_object_or_404(Materia, id=id_materia)
    docente=get_object_or_404(Docente, id=id_docente)
    md=get_object_or_404(Materia_docente, materia_id=id_materia,docente_id=id_docente)
    md.delete()
    return redirect('preinscripciones:lista_materias')

# Metodo que permite eliminar una relacion de aspirante inscrito en un grupo.
@permission_required("auth.administrador_permiso", raise_exception=True)
def eliminar_aspirante_grupo(request,pk):
    grupo_aspirante=get_object_or_404(Grupo_Aspirante,id=pk)
    grupo=grupo_aspirante.grupo
    aspirante=grupo_aspirante.aspirante

    grupo_aspirante.delete()
    
    cupos_usados=len(Grupo_Aspirante.objects.filter(grupo=grupo))
    grupo.set_cupos_restantes(cupos_usados=cupos_usados)
    
    aspirante.grupo=None
    aspirante.save()

    return ver_lista_alumnos_grupo(request,grupo.id)

# Clase que permite eliminar una materia especifica.
class eliminar_materia(PermissionRequiredMixin,DeleteView):
    permission_required="auth.administrador_permiso"
    model=Materia
    success_url=reverse_lazy("preinscripciones:lista_materias")