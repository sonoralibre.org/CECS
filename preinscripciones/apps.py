from django.apps import AppConfig


class PreinscripcionesConfig(AppConfig):
    name = 'preinscripciones'
