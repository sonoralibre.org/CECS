CREATE DATABASE CECS;
CREATE USER 'cecsuser'@'localhost' identified by 'cecs';
GRANT ALL PRIVILEGES ON CECS.* TO 'cecsuser'@'localhost';
FLUSH PRIVILEGES;