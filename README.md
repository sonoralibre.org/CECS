# Control Escolar Ciencias de la Salud (CECS)

## Prerrequisitos
- Crear un nuevo entorno virtual
El sistema tendrá que correr en un entorno virtual provisto por la herramienta virtualenv, sin embargo primero tenemos que instalar dicha herramienta.
Primeramente instalamos pip que nos ayuda a instalar herramientas:
```shell
   $ sudo apt update

   $ sudo apt install python3-pip
   ```
Una vez instalado pip instalamos la herramienta virtualenv:
```shell
   $ sudo pip install virtualenv
   ```
Despues creamos el nuevo entorno virtual y lo activamos:
```shell
   $ cd .

   $ virtualenv env_cecs

   $ . env_cecs/bin/activate
   ```

- Clonar el repositorio
Instalamos git en caso de no tenerlo:
```shell
   $ sudo apt-get install git
   ```
Nos movemos al directorio donde vamos a almacenar el proyecto, una vez ahí ejecutamos las siguientes lineas:
```shell
   $ git clone https://gitlab.com/gerashdo/CECS

   $ cd CECS
   ```

- Instalar servidor de bases de datos (MariaDB)
Necesitamos instalar el servidor y el cliente para podernos conectar desde el sistema, lo hacemos con las siguientes lineas:
```shell
   $ sudo apt-get install mariadb-server -y

   $ sudo apt-get install libmariadbclient-dev
   ```

- Instalar dependencias con archivo requirements.
Dentro de la carpeta raíz tenemos un archivo llamado requirements.txt el cual contiene todas las dependencias que necesita el proyecto para correr, ejecutamos la siguiente linea:
```shell
   $ pip install -r requeriments.txt
   ```
Dicha linea instalará todo lo necesario.

- Base de datos
Se tendrá que crear una base de datos llamada CECS y un usuario para poder conectarse a ella. Dentro del directorio raiz del proyecto hay un archivo llamado db.sql que nos ayudará a crear esto de manera automatica. 
Lo ejecutamos con la siguiente linea:
```shell
   $ sudo mysql -u root < db.sql
   ```

- Realizar migraciones
Para crear las entidades en la base de datos tenemos que correr las siguientes lineas:
```shell
   $ python3 manage.py makemigrations

   $ python3 manage.py migrate
   ```

- Crear permisos extra para el sistema
Ejecutar un script de python que se encuentra en el directorio raiz llamado script_permisos.py con la siguiente linea:
```shell
   $ python3 script_permisos.py
   ```

## Probar el proyecto
Una vez instaladas todas las dependencias y configurado todo lo necesario verificaremos que el proyecto corre de manera correcta.

- Ejecutar el sistema
Debido a que la lógica de negocio exige que las cuentas tienen que ser autenticadas será de utilidad crear un super usuario primeramente, lo cual se podrá hacer con la siguiente linea y siguiendo las instrucciones:
```shell
   $ python3 manage.py createsuperuser
   ```
Una vez con el super usuario ejecutamos la siguiente linea para correr el sistema:
```shel
   $ python3 manage.py runserver 0.0.0.0:8000 
   ```
Si el sistema está corriendo de manera correcta podremos acceder de la siguiente manera:
   > http://localhost:8000/login/

Lo cual debe de mostrar la pantalla de login del sistema.

## Versión

1.0.0 - Junio 2021

## Autores

* **Gerardo Jiménez Arguelles**
* **Oscar Leonardo Corvera Espinosa**
* **Ricardo Flores Vazquez**
* **José de Jesús Jiménez Arguelles**
